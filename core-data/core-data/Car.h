//
//  Car.h
//  core-data
//
//  Created by user on 2014/9/1.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class UserData;

@interface Car : NSManagedObject

@property (nonatomic, retain) NSString * plate;
@property (nonatomic, retain) UserData *belongto;

@end
