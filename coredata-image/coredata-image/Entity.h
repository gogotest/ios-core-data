//
//  Entity.h
//  coredata-image
//
//  Created by user on 2014/9/1.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Entity : NSManagedObject

@property (nonatomic, retain) id image;

@end
